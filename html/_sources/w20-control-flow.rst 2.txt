Week 20 - Control Flow
======================

This week we will look at different ways of running code in parallel in Python.

A Sequential Approach
---------------------

First we will look at running some code in a sequential way to establish a baseline.

The task at hand is downloading a large number of files.
Download these files: `https://flagpedia.net/data/flags/w2560.zip <https://flagpedia.net/data/flags/w2560.zip>`_

.. note:: While you can do the following from the computer you are working on, you will see a much more reaslistic effect if you can use another computer on your network or on the Internet.

.. warning:: Be careful - you could trigger a Denial of Service attack (or be suspected of doing so) if you run the code in this lecture on Internet servers that you do not control yourself.

Unzip the files into a folder called `w2560`.
Now go to that folder in a terminal and start a Python web server:

.. code-block:: bash

    $ python3 -m http.server 8000

Make a quick test to see if it works:

.. code-block:: bash

   $ http get http://cuda:8000/dk.png

   HTTP/1.0 200 OK
   Content-Length: 4879
   Content-type: image/png
   Date: Fri, 08 May 2020 12:35:26 GMT
   Last-Modified: Thu, 07 May 2020 15:31:40 GMT
   Server: SimpleHTTP/0.6 Python/3.6.9
   +-----------------------------------------+
   | NOTE: binary data not shown in terminal |
   +-----------------------------------------+

.. note:: You need to have HTTPie installed for this to work (`httpie.org <http://httpie.org>`_) but you can do the same thing with `curl` or `wget`.

.. note:: In the example above *cuda* is the name of the computer I am accessing. You can change this to *127.0.0.1* if you are running locally, or some other computer on your network.

Now let's make a program that downloads all the files.

.. code-block:: python

    # file: flags.py

    import os
    import time
    import sys

    import requests


    #BASE_URL = 'http://127.0.0.1:8000'
    BASE_URL = 'http://cuda:8000'
    SOURCE_DIR = 'w2560/'
    DEST_DIR = 'downloads/'

    def flag_list():
        return [f for f in os.listdir(SOURCE_DIR) if os.path.isfile(os.path.join(SOURCE_DIR, f))]

    def save_flag(img, filename):
        path = os.path.join(DEST_DIR, filename)
        with open(path, 'wb') as fp:
            fp.write(img)

    def get_flag(cc):
        url = f'{BASE_URL}/{cc}'
        response = requests.get(url)
        return response.content

    def show(text):
        print(text, end=' ')
        sys.stdout.flush()
        
    def download_many(cc_list):
        for cc in sorted(cc_list):
            image = get_flag(cc)
            show(cc)
            save_flag(image, f'{cc.lower()}')
        return len(cc_list)

    def main(download_many):
        t0 = time.time()
        count = download_many(flag_list())
        t1 = time.time() - t0
        print(f'\n{count} flags downloaded in {t1:.2f}sec.')

    if __name__ == '__main__':
        main(download_many)


Let's break the program down a bit.

The `download_many` function is fed as a parameter to the `main` function so that we can reuse some of the code in the coming examples.

The `flags.py` file reads the file list from a local copy of the `w2560` folder - this is just to get the names.
Therefore, there you also need to put a copy of the folder next to the script.

You also need to make a `downloads/` folder in the same directory as you are running the script from.
This is just so that the files can be put somewhere - you can delete this folder afterwards.

Then the files are downloaded sequentially, that is one by one, and we wait for one download to finish before we start the next.

The first run will probably be slow - this is because the web server is running with a *cold cache*.
Disregard the first run and make three more runs - you should get very similar results.
Calculate the average runtime.

It took about 8.3 seconds on average running the script in my setup - I downloaded from a single-board computer using a MicroSD card for storage, if you run it off you local computer you will get much faster results.

Concurrency and Futures
-----------------------

In our next attempt we are going to use `futures` from `concurrency`.

.. note:: Python also implements lower level libraries such as *threading* and *multiprocessing*. These works very much as you would expect, and we are not going to deal with them in this course as there are more elegant ways of dealing with threads and processes, unless you only need a single new thread or process. See the documentation at `https://docs.python.org/3/library/threading.html <https://docs.python.org/3/library/threading.html>`_ and `https://docs.python.org/3/library/multiprocessing.html <https://docs.python.org/3/library/multiprocessing.html>`_ respectively.

`concurrency` implements a `ThreadPoolExecutor` and a `ProcessPoolExecutor` with identical interfaces, making then easily interchangeable.

In general, you want to use `ThereadPoolExecutor` for centric I/O tasks and `ProcessPoolExecutor` for CPU centric tasks.

The implementation below is very similar to what you learned in 3rd semester using Java.

.. code-block:: python

    # file: flags_threadpool.py

    from concurrent import futures

    from flags import save_flag, get_flag, show, main, flag_list


    MAX_WORKERS = 20


    def download_one(cc):
        image = get_flag(cc)
        show(cc)
        save_flag(image, cc.lower())
        return cc

    def download_many(cc_list):
        workers = min(MAX_WORKERS, len(flag_list()))
        with futures.ThreadPoolExecutor(workers) as executor:
            res = executor.map(download_one, sorted(flag_list()))
        return len(list(res))

    if __name__ == '__main__':
        main(download_many)

The script reuses several functions from our previous implementation, so it's quite short.

The `map` function takes a function and an iterable, and applies the function to each element in the iterable.
In our case, we apply the `download_one` function to each of the files in the list returned from `flag_list`.

Again, run once and disregard the first result, then run the script three times and calculate the average.

On my computer it took about 2.4 seconds to download all the files, quite an improvement over the 8.3 seconds in the sequential implementation.

.. note:: The *MAX_WORKERS* value was chosen without much thought. Find an optimal value for your system.

.. note:: There is an obvious *bug* in the *download_many* implementation. Fix it so we don't get embarrased.

.. note:: Try to make an equivalent implementation of the example above with a *ProcessPoolExecutor* instead. What results do you get, and why?

There are no special requirements to the code we can run using a `ThreadPoolExecutor` or a `ProcessPoolExecutor`.
As an example, they can contain blocking code.
There is, however, an overhead associated with setting op new threads and processes.

.. note:: Try to implement our simple Mandelbrot implementation using the *concurrent* library. What kind of pool executor is best suited for the job, and what kind of results do you get? How do you divide the task?

Blocking I/O and the GIL
^^^^^^^^^^^^^^^^^^^^^^^^

The CPython interpreter is not internally thread-safe, so the Global Interpreter Lock (GIL) only allows one thread to execute at any given time.
We have no means of controlling the GIL when running Python programs.
However, code implemented in C can manage the GIL and launch its own threads via the operating system.
This way libraries such as Numpy that we saw earlier in the course can take advantage of multiple CPU cores and advanced processor capabilities such as Same Instruction Multiple Data (SIMD).

However, all standard library functions that performs blocking I/O release the GIL while waiting for I/O from the operating system.
This means that I/O bound programs can benefit from using threads at Python level: while waiting the result from an I/O call the blocked I/O function releases the GIL allowing another thread to run.

Concurrency with asyncio
------------------------

It's time for downloading our flags one last time.
The script below use the modern (as of Python 3.5) syntax `async` and `await`.
The new syntax is a replacement for coroutine decorator `@asyncio.coroutine` and `yield from` - addressing the issue that was also raised in class last week, that the `yield from` syntax is not very readable and not very obvious.

It is, however, still coroutines that implement the functionality, yielding control back when waiting for blocking calls.

Enough with the talk, let's see it in action:

.. code-block:: python

    # file: flags_async.py

    import asyncio
    import time

    import requests

    from flags import flag_list, show, save_flag


    BASE_URL = 'http://cuda:8000'
    SOURCE_DIR = 'w2560'
    DEST_DIR = 'downloads/'


    async def download_one(cc):
        show(cc)
        url = f'{BASE_URL}/{cc}'
        future = loop.run_in_executor(None, requests.get, url)
        response = await future
        save_flag(response.content, cc)

    async def main():
        t0 = time.time()
        for cc in flag_list():
            await download_one(cc)
        t1 = time.time() - t0
        print(f'\n{t1:.2f}sec')

    if __name__ == '__main__':
        loop = asyncio.get_event_loop()
        loop.run_until_complete(main())

The runtime for this script is similar to our first sequential attempt.
What is wrong?

.. note:: Try to figure out why the asynchronous script is not faster than the sequential implementation.

The problem is that we are waiting for the result before we submit the next download.
Let's fix that.

.. code-block:: python

    # file: flags_async2.py

    import asyncio
    import time

    import requests

    from flags import flag_list, show, save_flag


    BASE_URL = 'http://cuda:8000'
    SOURCE_DIR = 'w2560'
    DEST_DIR = 'downloads/'


    async def main():
        t0 = time.time()
        futures = []
        for cc in flag_list():
            show(cc)
            url = f'{BASE_URL}/{cc}'
            futures.append(loop.run_in_executor(None, requests.get, url))
        for future in futures:
            response = await future
            save_flag(response.content, response.url.split('/')[-1])
        t1 = time.time() - t0
        print(f'\n{t1:.2f}sec')

    if __name__ == '__main__':
        loop = asyncio.get_event_loop()
        loop.run_until_complete(main())

This time I get results slightly worse than the threaded implementation.
The results may vary - the more we can stack up and the longer before we start pulling the results the better.
If the files was much bigger or we had many more files to download the asyncio implementation would be a better choice than a multi-threaded implementation.

.. note:: For complete details see `https://docs.python.org/3/library/asyncio-task.html <https://docs.python.org/3/library/asyncio-task.html>`_  and `https://www.python.org/dev/peps/pep-0492/ <https://www.python.org/dev/peps/pep-0492/>`_.

Note that we are obtaining the event loop from asyncio.
It's now pretty clear what's going on: we are submitting a task to an event loop that simply spawns all the download jobs.
We don't have to concern ourselves with the number of workers or any of the complexity that comes along with multi-threading.
Asyncio pretty much guarantees that we will only spend the CPU time processing I/O tasks that is actually needed.
The overhead is also much smaller, because we don't spawn new threads or processes, we simply just don't waste the time that the CPU is waiting for I/O tasks to finish.

As you can see, cooperative multitasking (with its caveats) can be a better choice than preemptive multitasking when dealing with I/O.
It does, however, depend on the tasks to give up control as to not block the system.

