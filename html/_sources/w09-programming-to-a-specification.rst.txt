Week 09 - Programming to a Specification
========================================

An important skill in programming is to be able to program to a specification.
This week you are going to do research on the area of *cellular automata*, and make an implementation based on the sources below.

`https://en.wikipedia.org/wiki/Elementary_cellular_automaton <https://en.wikipedia.org/wiki/Elementary_cellular_automaton>`_
`http://mathworld.wolfram.com/ElementaryCellularAutomaton.html <http://mathworld.wolfram.com/ElementaryCellularAutomaton.html>`_
`https://plato.stanford.edu/entries/cellular-automata/ <https://plato.stanford.edu/entries/cellular-automata/>`_
`https://www.wolframalpha.com/input/?i=rule+110 <https://plato.stanford.edu/entries/cellular-automata/>`_

.. note:: Do Mandatory Assignment 1.
