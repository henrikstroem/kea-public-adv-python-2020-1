Week 17 - Object Oriented Idioms
================================

Inheritance: For Good or For Worse
----------------------------------

The Trickiness of Subclassing Built-In Types
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Most of the object-oriented ideas you are used to from languages like Java and Python originates back to the Smalltalk language.
If you have a historical interest you should look into Alan Kay's "The Early History of Smalltalk" (not required reading).

`Kay, Alan C. "The early history of Smalltalk." History of programming languages---II. 1996. 511-598 <https://dl.acm.org/doi/pdf/10.1145/234286.1057828>`_.

The Smalltalk developers saw inheritance as a way for novices to build on frameworks that could only be built by experts.
In other words, there's a distinction between subclassing framework classes and building your own class hierarchies.
Novice programmers often go too far with inheritance, and many problems should be solved in different ways.

Python differs from Java in many ways, object-wise most importantly:

- you don't have to put everything into classes in Python
- Python supports multiple-inheritance
- Python naturally support other paradigms
- Python didn't originally allow subclassing of some built-in types (i.e. `list` or `dict`)

In fact, there are still some caveats in subclassing some built-in types due to the fact that these are written in C rather than Python.
The abstract base classes (ABCs) are really only there for reference.

Let's see an example:

.. code-block:: python

   class DoppelDict(dict):
      def __setitem__(self, key, value):
         return super().__setitem__(key, [value] * 2)

The `__setitem__` calls the super (parent) class counterpart - we make a change to the data so we can see if our implementation is called or not.

.. code-block:: python

   >>> dd = DoppelDict(one=1)
   >>> dd
   {'one': 1}
   >>> dd['two'] = 2
   >>> dd
   {'one': 1, 'two': [2, 2]}
   >>> dd.update(three=3)
   >>> dd
   {'one': 1, 'two': [2, 2], 'three': 3}

As can be seen from the example above, our custom `__setitem__` method is only called when we explicitly invoke this method.
In probably all other object-oriented languages the overwritten method would have been called because the search for the implementation would start with `self` and then follow the class hierarchy.

Let's see another example:

.. code-block:: python

   class AnswerDict(dict):
      def __getitem__(self, key):
         return 42

In this example we have a custom `__getitem__`, so let's see when that will be called:

.. code-block:: python

   >>> ad = AnswerDict(a='foo')
   >>> ad['a']
   42
   >>> d = {}
   >>> d.update(ad)
   >>> d['a']
   'foo'
   >>> d
   {'a': 'foo'}

The problem here is a little more subtle, but the `update` didn't get the elements out of `ad` with the custom implementation of `__getitem__`.

.. note:: Python implements *UserDict*, *UserList*, and *UserString* in *collections* that you can inherit from if needed.
   These behave as you would expect.

.. warning:: As you can tell, to avoid the problems seen above, never inherit from built-ins like *dict*, *list*, and *str*.

.. note:: Do Assignment 16.

Multiple Inheritance and Method Resolution Order
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Programming languages that allow multiple inheritances need to deal with the problem of elements being implemented multiple times.
Let's see an example:

.. figure:: _static/diamond.png
    :align: center
    :alt: Diamond Inheritance
    :figclass: align-center

    Diamond Inheritance

.. code-block:: python

   # file: diamond.py

   class A:
      def ping(self):
         print('ping:', self)


   class B(A):
      def pong(self):
         print('pong:', self)


   class C(A):
      def pong(self):
         print('PONG:', self)


   class D(B, C):
      def ping(self):
         super().ping()
         print('post-ping:', self)

      def pingpong(self):
         self.ping()
         super().ping()
         self.pong()
         super().pong()
         C.pong(self)

.. code-block:: python

   >>> from diamond import *
   >>> d = D()
   >>> d.pong()
   pong: <diamond.D object at 0x105668ca0>
   >>> C.pong(d)
   PONG: <diamond.D object at 0x105668ca0>

The example above demonstrates how multiple inheritances are handled in Python.
The method resolution order goes as follows:

- `self`
- first `super`
- second `super`
- etc.

It also demonstrates how you can always call a method on a superclass directly, in this case: `C.pong(d)`.

A class only know about its direct superclass, so implementations in superclasses' superclasses are handled by those superclasses.
Let's see an example:

.. code-block:: python

   class A: pass

   class B: pass

   class C: pass

   class D(B, C):
      def __init__(self):
         print("MRO: ", self.__class__.__mro__)
         for base in self.__class__.__bases__:
               print("Base class: ", base)


   if __name__ == '__main__':
      d = D()

When we run this we get:

.. code-block:: bash

   MRO:  (<class '__main__.D'>, <class '__main__.B'>, <class '__main__.C'>, <class 'object'>)
   Base class:  <class '__main__.B'>
   Base class:  <class '__main__.C'>

MRO is the *Method Resolution Order*.
As you can see, `D` does not know about `A`.

.. warning:: In Python, as in many other languages, many problems are better solved by other means than inheritance.
   Composition is very often a better design choice, as has also been the case in most examples in this course.
   The Python Data Model works extremely well with composition.

Operator Overloading: Doing It Right
------------------------------------

Python has several kinds of operators:

- infix operators such as `+` or `|`
- unary operators such as `-` or `~`
- invocation operators: `()`
- attribute access operator: `.`
- item access and slicing operators: `[]`
- rich comparison operators: `==`, `<`, `>`, `<=` etc.

We are going to focus on infix and unary operators in this section.

Operator overloading is the concept of making operators work with your custom types.

.. warning:: Many people get a little too eager to use operator overloading for all kinds of things.
   You should be critical and only use operator overloading when it make sense.
   If you are in doubt, ask somebody else if they understand your code.

These rules apply for operator overloading in Python:

- you can't overload operators for built-in types
- you can't create new operators, only overload existing operators
- a few operators can't be overloaded: `is`, `and`, `or`, and `not` (but the bitwise operators `&`, `|`, and `~` can)

Unary Operators
^^^^^^^^^^^^^^^

Unary operators are operators that applies to just one object.
Therefore, these operators always only take `self` as an argument.

These are the unary operators in Python:

- `- __neg__`: arithmetic unary negation, e.g. if `x == 2` then `-x == 2`
- `+ __pos__`: arithmetic unary plus, e.g. `x == +x` (in most cases!!)
- `~ __invert__`: bitwise inverse, e.g. `~x == -(1 - x)`, so `x == 2` gives `~x == -3`
- `abs() __abs__`: absolute value or magnitude

These unary operators are very easy to implement, as we have already seen.

.. note:: The unary operators should never mutate the object instance.
   Always return a new instance instead of a suitable type.

.. note:: If you wonder when *x == +x* would not be the case, see the example at the very bottom of this page.

Operators such as `+` and `-` should probably return an instance of the same type as `self`, whereas `abs` is usually a scalar.

.. note:: Do Assignment 17.

Let's take a look at our implementation of a vector where we left it last time:

.. code-block:: python

   from array import array
   import reprlib
   import math
   import numbers
   import functools
   import operator


   class Vector:
      """
         >>> v = Vector([1, 2, 3, 4])
         >>> list(v)
         [1.0, 2.0, 3.0, 4.0]
         >>> v = Vector((1, 2))
         >>> list(v)
         [1.0, 2.0]
         >>> v = Vector([3, 4, 5])
         >>> len(v)
         3
         >>> v[0], v[-1]
         (3.0, 5.0)
         >>> v7 = Vector(range(7))
         >>> v7[1:4]
         Vector([1.0, 2.0, 3.0])
         >>> v5 = Vector(range(5))
         >>> v5.x
         0.0
         >>> v5.y, v5.z, v5.t
         (1.0, 2.0, 3.0)
      """

      typecode = 'd'
      shortcut_names = 'xyzt'

      def __init__(self, components):
         self._components = array(self.typecode, components)

      def __iter__(self):
         return iter(self._components)

      def __repr__(self):
         components = reprlib.repr(self._components)
         components = components[components.find('['):-1]
         return 'Vector({})'.format(components)

      def __str__(self):
         return str(tuple(self))
      
      def __bytes__(self):
         return (bytes([ord(self.typecode)]) +
                  bytes(self._components))
      
      def __eq__(self, other):
         return len(self) == len(other) and all(a == b for a, b in zip(self, other))

      def __hash__(self):
         hashes = (hash(x) for x in self._components)
         return functools.reduce(operator.xor, hashes, 0)

      def __abs__(self):
         return math.sqrt(sum(x * x for x in self))

      def __bool__(self):
         return bool(abs(self))

      def __len__(self):
         return len(self._components)

      def __getitem__(self, index):
         cls = type(self)
         if isinstance(index, slice):
               return cls(self._components[index])
         elif isinstance(index, numbers.Integral):
               return self._components[index]
         else:
               raise TypeError(f'{cls.__name__} indices must be integers.')

      def __getattr__(self, name):
         cls = type(self)
         if len(name) == 1:
               pos = cls.shortcut_names.find(name)
               if 0 <= pos < len(self._components):
                  return self._components[pos]
         raise AttributeError(f'{cls.__name__} object has no attribute {name}')

      def __setattr__(self, name, value):
         cls = type(self)
         if len(name) == 1:
               if name in cls.shortcut_names:
                  error = f'Attribute {name} is read only.'
               elif name.islower():
                  error = "Can't set attribute names 'a' to 'z' in {cls_name!r}"
               else:
                  error = ''
               if error:
                  msg = error.format(cls_name=cls.__name__)
                  raise AttributeError(msg)
         super().__setattr__(name, value)

      @classmethod
      def frombytes(cls, octets):
         typecode = chr(octets[0])
         memv = memoryview(octets[1:]).cast(typecode)
         return cls(memv)


   if __name__ == '__main__':
      import doctest
      doctest.testmod(verbose=True, optionflags=doctest.ELLIPSIS)

We have already implemented `__abs__`, but we should also implement `__neg__` and `__pos__`:

.. code-block:: python

    def __abs__(self):
        return math.sqrt(sum(x * x for x in self))

    def __neg__(self):
        return Vector(-x for x in self)

    def __pos__(self):
        return Vector(self)

Note that in `__pos__` we are in fact returning a new instance rather than `self`.

Also, because of the Python Data Model we can deal with `self` as if we inherited a sequence type, when we in fact used composition.
Likewise, from the outside our vector implementation looks like it inherited a sequence type, because it behaves like one.

Overloading + for Vector Addition
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Our vector implementation is a sequence type.
As such, it's supposed to implement `+` for concatenation and `*` for repetition, just like a list:

.. code-block:: python

   >>> l = [1, 2, 3, 4]
   >>> l + [5]
   [1, 2, 3, 4, 5]
   >>> l * 2
   [1, 2, 3, 4, 1, 2, 3, 4]

However, in our implementation it makes more sense for the vector to use these operators for addition and multiplication.
It's a perfectly sensible choice, because we foremost think of this type as a vector in a mathematical context.

Let's take a look at some use cases for our vector:

.. code-block:: python

   >>> v1 = Vector([3, 4, 5])
   >>> v2 = Vector([6, 7, 8])
   >>> v1 + v2
   Vector([9.0, 11.0, 13.0])
   >>> v1 + v2 == Vector([3+6, 4+7, 5+8])
   True

What should happen if we try to add two vectors of different length?
We could raise an error, but this is actually a quite common case, and in most instances we would rather pad the shortest vector with zeros:

.. code-block:: python

   >>> v1 = Vector([3, 4, 5, 6])
   >>> v3 = Vector([1, 2])
   >>> v1 + v3
   Vector([4.0, 6.0, 5.0, 6.0])

Go ahead and add these tests as doctests to the vector implementation.

.. note:: Do Assignment 18.

Infix operators, like unary operators, should never mutate `self` or `other` but return a new instance.

Let's try an implementation:

.. code-block:: python

   ...
   import itertools

   class Vector:
         ...
         def __add__(self, other):
            pairs = itertools.zip_longest(self, other, fillvalue=0.0)
            return Vector(a + b for a, b in pairs)

This implementation will pass the tests we have written so far.

Let's expand on this a bit - if we add the test:

.. code-block:: python

   >>> v1 = Vector([3, 4, 5])
   >>> v1 + [10, 20, 30]
   Vector([13.0, 24.0, 35.0])

This will still pass, however:

.. code-block:: python

   >>> [10, 20, 30] + v1
   Vector([13.0, 24.0, 35.0])

This will fail.

.. note:: Do Assignment 19.

The simplest implementation simply make use of the already existing `__add__` method:

.. code-block:: python

   def __radd__(self, other):
      return self + other

It might seem a little confusing that `__radd__` doesn't switch the order of the operands.
But this is because `self` always is the first argument passed to an instance method, so this can't match expectations.

This is all great, but consider this:

.. code-block:: text

   >>> v = Vector([1, 2, 3])
   >>> v + 1
   Traceback (most recent call last):
   File "<stdin>", line 1, in <module>
   File "/Users/henrik/Library/Mobile Documents/com~apple~CloudDocs/KEA/kea-private-adv-python/2020-1/py-src/week16/vector_06.py", line 92, in __add__
      pairs = itertools.zip_longest(self, other, fillvalue=0.0)
   TypeError: 'int' object is not iterable

This error message is not very helpful, because it relates to our implementation rather than the external behavior of the vector class.

So, `1` is not iterable, but then consider this:

.. code-block:: text

   >>> v + 'ABC'
   Traceback (most recent call last):
   File "<stdin>", line 1, in <module>
   File "/Users/henrik/Library/Mobile Documents/com~apple~CloudDocs/KEA/kea-private-adv-python/2020-1/py-src/week16/vector_06.py", line 93, in __add__
      return Vector(a + b for a, b in pairs)
   File "/Users/henrik/Library/Mobile Documents/com~apple~CloudDocs/KEA/kea-private-adv-python/2020-1/py-src/week16/vector_06.py", line 52, in __init__
      self._components = array(self.typecode, components)
   File "/Users/henrik/Library/Mobile Documents/com~apple~CloudDocs/KEA/kea-private-adv-python/2020-1/py-src/week16/vector_06.py", line 93, in <genexpr>
      return Vector(a + b for a, b in pairs)
   TypeError: unsupported operand type(s) for +: 'float' and 'str'

`'ABC'` is definitely iterable, but it still doesn't work, and again, the error message relates to our implementation.

.. note:: Do Assignment 20.

So how should we handle the problem?

In the first case we do have the option to broadcast the value `1` into the vector type.
This is the behavior we find in frameworks like NumPy and Pandas.
It would be something like this:

.. code-block:: python

   >>> v = Vector([1, 2, 3])
   >>> v + 1
   Vector([2.0, 3.0, 4.0])

That actually makes sense, but we will not implement this yet.

The second case is more obvious, because adding a `str` to a vector makes very little sense.

That brings us to something like this:

.. code-block:: python

   def __add__(self, other):
      try:
         pairs = itertools.zip_longest(self, other, fillvalue=0.0)
         return Vector(a + b for a, b in pairs)
      except TypeError:
         return NotImplemented

   def __radd__(self, other):
      return self + other

We get a meaningful error message that relates to the external behavior of the vector class rather than its implementation:

.. code-block:: text

   >>> v = Vector([1, 2, 3])
   >>> v + 1
   Traceback (most recent call last):
   File "<stdin>", line 1, in <module>
   TypeError: unsupported operand type(s) for +: 'Vector' and 'int'
   >>> 
   >>> v + 'ABC'
   Traceback (most recent call last):
   File "<stdin>", line 1, in <module>
   TypeError: unsupported operand type(s) for +: 'Vector' and 'str'

Overloading * for Scalar Multiplication
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Next up is the `*` operator.
This is a bit more ambiguous, as it could be element-wise multiplication or the dot product.

We will stick with element-wise multiplication, also called the scalar product.

.. note:: Do Assignment 21.

We will start off by writing the tests:

.. code-block:: python

   >>> v = Vector([0, 1, -2])
   >>> v * 3
   Vector([0.0, 3.0, -6.0])
   >>> 3 * v
   Vector([0.0, 3.0, -6.0])

We will start with a naïve implementation that will pass the tests:

.. code-block:: python

   def __mul__(self, scalar):
      return Vector(n * scalar for n in self)

   def __rmul__(self, scalar):
      return self * scalar

That works, but like before, if it breaks we will get some very unhelpful error messages.

We can do better than that, so let's try:

.. code-block:: python

   def __mul__(self, scalar):
      if isinstance(scalar, numbers.Real):
         return Vector(n * scalar for n in self)
      else:
         return NotImplemented

   def __rmul__(self, scalar):
      return self * scalar

Now we get some reasonable errors:

.. code-block:: text

   >>> v = Vector([1, 2, 3])
   >>> v * v
   Traceback (most recent call last):
   File "<stdin>", line 1, in <module>
   TypeError: unsupported operand type(s) for *: 'Vector' and 'Vector'
   >>> v * 'ABC'
   Traceback (most recent call last):
   File "<stdin>", line 1, in <module>
   TypeError: can't multiply sequence by non-int of type 'Vector'

Overloading @ for Matrix Multiplication
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Since Python 3.5 the operator `@` has been reserved for matrix multiplication or *dot product*.

Let's see a test that should pass for this use:

.. code-block:: python

   >>> v1 = Vector([1, 2, 3])
   >>> v2 = Vector([5, 6, 7])
   >>> v1 @ v2 == 38.0 # 1*5 + 2*6 +3*7
   True

.. note:: Do Assignment 22.

This time we will get straight to a proper implementation:

.. code-block:: python

   def __matmul__(self, other):
      try:
         return sum(a * b for a, b in zip(self, other))
      except TypeError:
         return NotImplemented

   def __rmatmul__(self, other):
      return self @ other


Rich Comparison Operators
^^^^^^^^^^^^^^^^^^^^^^^^^

In Python, these are the rich comparison operators: `==`, `!=`, `>`, `<`, `>=`, and `<=`.

They are similar to what we have seen so far, but:

- there is not separate left-hand and right-hand version
- for `==` and `!=` if the reverse call fails, Python compares object IDs instead of raising a `TypeError`

Instead of left-hand and right-hand calls there is a forward method call and a reverse method call:

.. code-block:: text

   a == b   fw: a.__eq__(b)   rev: b.__eq__(a)  fallback: id(a) == id(b)
   a != b   fw: a.__ne__(b)   rev: b.__ne__(a)  fallback: not (a == b)
   a > b    fw: a.__gt__(b)   rev: b.__lt__(a)  fallback: TypeError
   a < b    fw: a.__lt__(b)   rev: b.__gt__(a)  fallback: TypeError
   a >= b   fw: a.__ge__(b)   rev: b.__le__(a)  fallback: TypeError
   a <= b   fw: a.__le__(b)   rev: b.__ge__(a)  fallback: TypeError

The reverse methods are called when the initial method returns `NotImplemented`.

We can apply this knowledge to our vector implementation.

Currently our `__eq__` looks like this:

.. code-block:: python

   def __eq__(self, other):
      return len(self) == len(other) and all(a == b for a, b in zip(self, other))

It has this strange behavior that we have discussed earlier:

.. code-block:: python

   >>> v = Vector([1.0, 2.0, 3.0])
   >>> v == (1, 2, 3)
   True

If we see how this kind of thing is handled elsewhere in Python:

.. code-block:: python

   >>> [1, 2] == (1, 2)
   False

So let's fix that.

We want these tests to pass:

.. code-block:: python

   >>> v1 = Vector([1, 2, 3])
   >>> v2 = Vector([1, 2, 3])
   >>> v1 == v2
   True
   >>> v1 == (1, 2, 3)
   False
   >>> v1 == [1, 2, 3]
   False

.. note:: Do Assignment 23.

.. code-block:: python

   def __eq__(self, other):
      if isinstance(other, Vector):
         return len(self) == len(other) and all(a == b for a, b in zip(self, other))
      else:
         return NotImplemented

Augmented Assignment Operators
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Augmented assignment operators are `+=`, `-=`, `*=`, `/=` etc.

Our vector class already supports these operators, but with a caveat:

.. code-block:: python

   >>> v = Vector([1, 2, 3])
   >>> id(v)
   4319272096
   >>> v_alias = v
   >>> id(v_alias)
   4319272096
   >>> v += Vector([2, 3, 4])
   >>> v
   Vector([3.0, 5.0, 7.0])
   >>> id(v)
   4319688880
   >>> id(v_alias)
   4319272096

As you can see, using an augmented assignment operator creates a new object.
This is because for the moment, these act like syntactic sugar, in that `a += b` is evaluated as `a = a + b`.

.. note:: Do Assignment 24.

As you remember from our discussion about mutation, `a = a + b` does not return a mutated object but rather a new object.
Because our vector is hashable, and therefore must be immutable, that is actually the behavior we are interested in.
Thus, we should not implement `__iadd__`.

Full Listing of Vector
^^^^^^^^^^^^^^^^^^^^^^

Below you will find the full listing of the vector class.

.. note:: Do Assignment 25.

.. code-block:: python

   from array import array
   import reprlib
   import math
   import numbers
   import functools
   import operator
   import itertools


   class Vector:
      """
         >>> v = Vector([1, 2, 3, 4])
         >>> list(v)
         [1.0, 2.0, 3.0, 4.0]
         >>> v = Vector((1, 2))
         >>> list(v)
         [1.0, 2.0]
         >>> v = Vector([3, 4, 5])
         >>> len(v)
         3
         >>> v[0], v[-1]
         (3.0, 5.0)
         >>> v7 = Vector(range(7))
         >>> v7[1:4]
         Vector([1.0, 2.0, 3.0])
         >>> v5 = Vector(range(5))
         >>> v5.x
         0.0
         >>> v5.y, v5.z, v5.t
         (1.0, 2.0, 3.0)
         >>> v1 = Vector([3, 4, 5])
         >>> v2 = Vector([6, 7, 8])
         >>> v1 + v2
         Vector([9.0, 11.0, 13.0])
         >>> v1 + v2 == Vector([3+6, 4+7, 5+8])
         True
         >>> v1 = Vector([3, 4, 5, 6])
         >>> v3 = Vector([1, 2])
         >>> v1 + v3
         Vector([4.0, 6.0, 5.0, 6.0])
         >>> v1 = Vector([3, 4, 5])
         >>> v1 + [10, 20, 30]
         Vector([13.0, 24.0, 35.0])
         >>> [10, 20, 30] + v1
         Vector([13.0, 24.0, 35.0])
         >>> v = Vector([0, 1, -2])
         >>> v * 3
         Vector([0.0, 3.0, -6.0])
         >>> 3 * v
         Vector([0.0, 3.0, -6.0])
         >>> v1 = Vector([1, 2, 3])
         >>> v2 = Vector([5, 6, 7])
         >>> v1 @ v2 == 38.0 # 1*5 + 2*6 +3*7
         True
         >>> v1 = Vector([1, 2, 3])
         >>> v2 = Vector([1, 2, 3])
         >>> v1 == v2
         True
         >>> v1 == (1, 2, 3)
         False
         >>> v1 == [1, 2, 3]
         False
      """

      typecode = 'd'
      shortcut_names = 'xyzt'

      def __init__(self, components):
         self._components = array(self.typecode, components)

      def __iter__(self):
         return iter(self._components)

      def __repr__(self):
         components = reprlib.repr(self._components)
         components = components[components.find('['):-1]
         return 'Vector({})'.format(components)

      def __str__(self):
         return str(tuple(self))
      
      def __bytes__(self):
         return (bytes([ord(self.typecode)]) +
                  bytes(self._components))
      
      def __eq__(self, other):
         if isinstance(other, Vector):
               return len(self) == len(other) and all(a == b for a, b in zip(self, other))
         else:
               return NotImplemented

      def __hash__(self):
         hashes = (hash(x) for x in self._components)
         return functools.reduce(operator.xor, hashes, 0)

      def __abs__(self):
         return math.sqrt(sum(x * x for x in self))

      def __neg__(self):
         return Vector(-x for x in self)

      def __pos__(self):
         return Vector(self)

      def __bool__(self):
         return bool(abs(self))

      def __len__(self):
         return len(self._components)

      def __add__(self, other):
         try:
               pairs = itertools.zip_longest(self, other, fillvalue=0.0)
               return Vector(a + b for a, b in pairs)
         except TypeError:
               return NotImplemented

      def __radd__(self, other):
         return self + other

      def __mul__(self, scalar):
         if isinstance(scalar, numbers.Real):
               return Vector(n * scalar for n in self)
         else:
               return NotImplemented

      def __rmul__(self, scalar):
         return self * scalar

      def __matmul__(self, other):
         try:
               return sum(a * b for a, b in zip(self, other))
         except TypeError:
               return NotImplemented

      def __rmatmul__(self, other):
         return self @ other

      def __getitem__(self, index):
         cls = type(self)
         if isinstance(index, slice):
               return cls(self._components[index])
         elif isinstance(index, numbers.Integral):
               return self._components[index]
         else:
               raise TypeError(f'{cls.__name__} indices must be integers.')

      def __getattr__(self, name):
         cls = type(self)
         if len(name) == 1:
               pos = cls.shortcut_names.find(name)
               if 0 <= pos < len(self._components):
                  return self._components[pos]
         raise AttributeError(f'{cls.__name__} object has no attribute {name}')

      def __setattr__(self, name, value):
         cls = type(self)
         if len(name) == 1:
               if name in cls.shortcut_names:
                  error = f'Attribute {name} is read only.'
               elif name.islower():
                  error = "Can't set attribute names 'a' to 'z' in {cls_name!r}"
               else:
                  error = ''
               if error:
                  msg = error.format(cls_name=cls.__name__)
                  raise AttributeError(msg)
         super().__setattr__(name, value)

      @classmethod
      def frombytes(cls, octets):
         typecode = chr(octets[0])
         memv = memoryview(octets[1:]).cast(typecode)
         return cls(memv)


   if __name__ == '__main__':
      import doctest
      doctest.testmod(verbose=True, optionflags=doctest.ELLIPSIS)

The Curious Case of +x
^^^^^^^^^^^^^^^^^^^^^^

As promised earlier, we would look at an example where `x == +x` is not true.

From a mathematical standpoint this makes very little sense, but we just have to accept that `+` is not an *identity function*.

In the example below we shift the *arithmetic context* from a precision of 40 to 28:

.. code-block:: python

   >>> import decimal
   >>> ctx = decimal.getcontext()
   >>> 
   >>> ctx.prec = 40
   >>> one_third = decimal.Decimal('1') / decimal.Decimal('3')
   >>> one_third
   Decimal('0.3333333333333333333333333333333333333333')
   one_third == +one_third
   >>> one_third == +one_third
   True
   >>> ctx.prec = 28
   >>> +one_third
   Decimal('0.3333333333333333333333333333')
   >>> one_third == +one_third
   False
   >>> ctx = decimal.getcontext()
   >>> ctx.prec = 10
   >>> one_t = decimal.Decimal('1') / decimal.Decimal('3')
   >>> one_t
   Decimal('0.3333333333')

The calculations depends on the level of precision, and numbers such as one third easily cause problems when numbers of different levels of precisions are compared.

See `this post on Stack Overflow <https://stackoverflow.com/a/28094734/790474>`_ for more on the arithmetic context precision.
