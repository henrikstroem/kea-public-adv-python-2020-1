Week 19 - Control Flow
======================

Coroutines
----------

If you look up the word *yield* in the dictionary, you will likely see two definitions: one meaning to *produce or provide*, and one meaning *giving way for arguments*.
So far, we have seen generator produce elements as an iterator.
In this lecture, we will see how coroutines implements the second definition from the dictionary.

.. note:: There are not any assignments in this lecture, but there are some activities throughout the lecture. It's important that you take time to experiment with the code examples - try to change things, print results mid way etc.

Basic Coroutines
^^^^^^^^^^^^^^^^

Let's start with an example:

.. code-block:: python

    def simple_coroutine():
        print('-> coroutine started')
        x = yield
        print('-> coroutine received:', x)

Note that the `yield` keyword is on the right side of the assignment.

.. code-block:: python

    >>> from simple_coroutine import simple_coroutine
    >>> 
    >>> my_coro = simple_coroutine()
    >>> next(my_coro)
    #-> coroutine started
    >>> my_coro.send(42)
    #-> coroutine received: 42
    #Traceback (most recent call last):
    #File "<stdin>", line 1, in <module>
    #StopIteration

A generator is a function that contains `yield` in its body;
a coroutine is a function that *receives data through yield* - consequently a coroutine is a generator.
While a generator only accepts input at initialization, a coroutine continues to accept input.

There is actually an implicit `x = yield None` when in the function above, but we don't have to write it out.

The `next(my_coro)` call is necessary to initialize the coroutine.
You can't make use of it before initialization:

.. code-block:: python

    >>> my_coro = simple_coroutine()
    >>> my_coro.send(42)
    #Traceback (most recent call last):
    #File "<stdin>", line 1, in <module>
    #TypeError: can't send non-None value to a just-started generator

Also note this behavior:

.. code-block:: python

    >>> my_coro = simple_coroutine()
    >>> next(my_coro)
    #-> coroutine started
    >>> next(my_coro)
    #-> coroutine received: None
    #Traceback (most recent call last):
    #File "<stdin>", line 1, in <module>
    #StopIteration

There is no loop in our coroutine, so it can only be called once.
However, it behaves just like any other generator for that one call.

A coroutine can be in one of four states:

- `GEN_CREATED` - waiting to start execution
- `GEN_RUNNING` - currently being executed
- `GEN_SUSPENDED` - currently suspended at a `yield` expression
- `GEN_CLOSED` - execution has completed

A call like `my_coro.send(42)` can only take place in `GEN_SUSPEDED` state.

Now let's look at a generator that yields more than once, so we can inspect these states:

.. code-block:: python

    def simple_coro2(a):
        print('-> started: a =', a)
        b = yield a
        print('-> received: b =', b)
        c = yield a + b
        print('-> received: c =', c)


.. code-block:: python

    >>> from inspect import getgeneratorstate
    >>>
    >>> my_coro2 = simple_coro2(14)
    >>> getgeneratorstate(my_coro2)
    'GEN_CREATED'
    >>> next(my_coro2)
    #-> started: a = 14
    14
    >>> getgeneratorstate(my_coro2)
    'GEN_SUSPENDED'
    >>> my_coro2.send(28)
    #-> received: b = 28
    42
    >>> getgeneratorstate(my_coro2)
    'GEN_SUSPENDED'
    >>> my_coro2.send(99)
    #-> received: c = 99
    #Traceback (most recent call last):
    #File "<stdin>", line 1, in <module>
    #StopIteration
    >>> getgeneratorstate(my_coro2)
    'GEN_CLOSED'

As you can see from the example above, coroutines can both receive and emit data via `yield`.

Coroutines can retain a state just like classes and closures; let's take advantage of that:

.. code-block:: python

    def averager():
        total = 0.0
        count = 0
        average = None
        while True:
            term = yield average
            total += term
            count += 1
            average = total/count

And this is how we would use it:

.. code-block:: python

    >>> coro_avg = averager()
    >>> next(coro_avg)
    >>> coro_avg.send(10)
    10.0
    >>> coro_avg.send(20)
    15.0

This is pretty nifty: in one call we update the average and receive it.

.. note:: Take a moment to think: the coroutine contains an infinite loop - how will it ever terminate?

Decorators for Coroutines
^^^^^^^^^^^^^^^^^^^^^^^^^

The initial call to the coroutine `next(my_coroutine)` or `my_coroutine.send(None)` is called *priming*.
It's an easy to forget chore, so let's see how we can avoid it.

.. code-block:: python

    from functools import wraps


    def coroutine(func):
        """Decorator: primes 'func' by advancing to first 'yield'"""
        @wraps(func)
        def primer(*args, **kwargs):
            gen = func(*args, **kwargs)
            next(gen)
            return gen
        return primer

For an easy-to-understand explanation on `functools.wraps` see this post on Stack Overflow: `https://stackoverflow.com/questions/308999/what-does-functools-wraps-do#309000 <https://stackoverflow.com/questions/308999/what-does-functools-wraps-do#309000>`_

The decorator above takes care of priming the coroutine, and returning it.
Now we can use it to decorate the `averager` function:

.. code-block:: python

    from coroutils import coroutine


    @coroutine
    def averager():
        total = 0.0
        count = 0
        average = None
        while True:
            term = yield average
            total += term
            count += 1
            average = total/count

Now we can use the `averager` function without priming - it goes directly into suspended mode:

.. code-block:: python

    >>> coro_avg = averager()
    >>> from inspect import getgeneratorstate
    >>> getgeneratorstate(coro_avg)
    'GEN_SUSPENDED'
    >>> coro_avg(10)
    #Traceback (most recent call last):
    #File "<stdin>", line 1, in <module>
    #TypeError: 'generator' object is not callable
    >>> coro_avg.send(10)
    10.0
    >>> coro_avg.send(15)
    12.5

Coroutine Termination and Exception Handling
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An exception in a coroutine will propagate to the caller of `next` or `send` as illustrated below:

.. code-block:: python

    >>> from averager import averager
    >>> coro_avg = averager()
    >>> coro_avg.send(40)
    40.0
    >>> coro_avg.send(50)
    45.0
    >>> coro_avg.send('spam')
    #Traceback (most recent call last):
    #File "<stdin>", line 1, in <module>
    #File "/Users/henrik/Library/Mobile Documents/com~apple~CloudDocs/KEA/kea-private-adv-python/2020-1/py-src/week19/averager.py", line 11, in averager
    #    total += term
    #TypeError: unsupported operand type(s) for +=: 'float' and 'str'
    >>> coro_avg.send(60)
    #Traceback (most recent call last):
    #File "<stdin>", line 1, in <module>
    #StopIteration

The coroutine did not handle the exception, thus it's propagated up the call stack.
Any attempt at using the coroutine afterwards will cause a `StopIteration` error.

The listing above hints at how the generator can ever be deallocated (as noted earlier - how to terminate a coroutine).
In earlier versions of Python many people would do something like `coro.send(None)` or `coro.send(StopIteration)`.
While this actually works, it's merely a consequence of the handling demonstrated above, rather than a designed behavior.

Later versions of Python includes two methods for generators: `generator.throw()` and `generator.close()`.

.. code-block:: python

    class DemoException(Exception):
        """An exception type for the demonstration."""

    def demo_exec_handling():
        print('# coroutine started')
        while True:
            try:
                x = yield
            except DemoException:
                print('# DemoException handled ... continuing.')
            else:
                print('# coroutine received:', x)
        raise RuntimeError('# This line should never run!')

Let's first see this acting as we have seen before:

.. code-block:: python

    >>> exc_coro = demo_exec_handling()
    >>> next(exc_coro)
    # coroutine started
    >>> exc_coro.send(11)
    # coroutine received: 11
    >>> exc_coro.close()
    >>> from inspect import getgeneratorstate
    >>> getgeneratorstate(exc_coro)
    'GEN_CLOSED'

The generator is not deallocated from memory - it just remains in closed state.

Now let's see what happens when the coroutine handles an exception:

.. code-block:: python

    >>> exc_coro = demo_exec_handling()
    >>> next(exc_coro)
    # coroutine started
    >>> exc_coro.send(11)
    # coroutine received: 11
    >>> exc_coro.throw(DemoException)
    # DemoException handled ... continuing.
    >>> exc_coro.send(11)
    # coroutine received: 11

Now, the coroutine handles the error, and the coroutine remains functional after the error has occured.

Next, let's throw an error at the coroutine that it doesn't handle:

.. code-block:: python

    >>> exc_coro = demo_exec_handling()
    >>> next(exc_coro)
    # coroutine started
    >>> exc_coro.send(11)
    # coroutine received: 11
    >>> exc_coro.throw(ZeroDivisionError)
    #Traceback (most recent call last):
    #File "<stdin>", line 1, in <module>
    #File "/Users/henrik/Library/Mobile Documents/com~apple~CloudDocs/KEA/kea-private-adv-python/2020-1/py-src/week19/coro_exec_demo.py", line 8, in demo_exec_handling
    #    x = yield
    #ZeroDivisionError
    >>> exc_coro.send(11)
    #Traceback (most recent call last):
    #File "<stdin>", line 1, in <module>
    #StopIteration

A coroutine often need to do some clean-up when finished.
In that case you can use the following construct:

.. code-block:: python

    class DemoException(Exception):
        """Just a demo exception"""

    def demo_finally():
        print('# coroutine started')
        try:
            while True:
                try:
                    x = yield
                except DemoException:
                    print('# DemoException handled - continuing.')
                else:
                    print('# coroutine received:', x)
        finally:
            print('# coroutine endning')

Let's see it in action:

.. code-block:: python

    >>> coro = demo_finally()
    >>> next(coro)
    # coroutine started
    >>> coro.send(11)
    # coroutine received: 11
    >>> coro.throw(DemoException)
    # DemoException handled - continuing.
    >>> coro.send(11)
    # coroutine received: 11
    >>> coro.close()
    # coroutine endning

Usually in Python, we are not concerned with deallocation of objects.
However, a coroutine might hold a significant amount of data, and you might want to deallocate it manually in some situation.
The listing below show how you can deallocate any object in Python:

.. code-block:: python

    >>> coro
    #<generator object demo_finally at 0x10bb76c10>
    >>> del(coro)
    >>> coro
    #Traceback (most recent call last):
    #File "<stdin>", line 1, in <module>
    #NameError: name 'coro' is not defined

Returning a Value from a Coroutine
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In some cases we'd rather accumulate a final result than emitting results with each call to a coroutine.
Look at the example below:

.. code-block:: python

    from collections import namedtuple


    Result = namedtuple('Result', 'count average')


    def averager():
        total = 0.0
        count = 0
        average = None
        while True:
            term = yield
            if term is None:
                break
            total += term
            count += 1
            average = total/count
        return Result(count, average)

We'll use it like before, but the result comes at the end:

.. code-block:: python

    >>> coro_avg = averager()
    >>> next(coro_avg)
    >>> coro_avg.send(10)
    >>> coro_avg.send(30)
    >>> coro_avg.send(6.5)
    >>> coro_avg.send(None)
    #Traceback (most recent call last):
    #File "<stdin>", line 1, in <module>
    #StopIteration: Result(count=3, average=15.5)

Sending `None` breaks the loop, and we return (not yield) the result at the end.
The result is somehow piggybacking on the `StopIteration` exception.
That makes it somehow difficult to obtain the result.

.. code-block:: python

    >>> coro_avg = averager()
    >>> coro_avg.send(None)
    >>> coro_avg.send(6.5)
    >>> coro_avg.send(30)
    >>> result = coro_avg.send(None)
    #Traceback (most recent call last):
    #File "<stdin>", line 1, in <module>
    #StopIteration: Result(count=2, average=18.25)
    >>> result
    #Traceback (most recent call last):
    #File "<stdin>", line 1, in <module>
    #NameError: name 'result' is not defined

.. note:: How can you get the result?

To obtain the result we have to do something like this:

.. code-block:: python

    >>> coro_avg = averager()
    >>> coro_avg.send(None)
    >>> coro_avg.send(30)
    >>> coro_avg.send(6.5)
    >>> try:
    ...     coro_avg.send(None)
    ... except StopIteration as exc:
    ...     result = exc.value
    ...
    >>> result
    Result(count=2, average=18.25)

This is really ugly, but there is a better way:

.. code-block:: python

    from collections import namedtuple


    Result = namedtuple('Result', 'count average')


    def averager():
        total = 0.0
        count = 0
        average = None
        while True:
            term = yield
            if term is None:
                break
            total += term
            count += 1
            average = total/count
        yield from Result(count, average)

Now it will consume the error and yield its result:

.. code-block:: python

    >>> coro_avg = averager()
    >>> coro_avg.send(None)
    >>> coro_avg.send(5)
    >>> coro_avg.send(7)
    >>> coro_avg.send(None)
    2

Using yield from
^^^^^^^^^^^^^^^^

.. note:: Things are about to get quite abstract. Don't despair! This is difficult to everybody, but it will help you understand modern concurrency in Python, promise! We will get to that next week, and then it will all make more sense.

`yield from` can do a lot more than you just saw.
Many Pythonistas argue that `yield from` should have had its own keyword, because the name is a bit misleading.

As an example, these two functions do exactly the same:

.. code-block:: python

    def gen_yield():
        for c in 'AB':
            yield c
        for i in range(5)
            yield i


    def gen_yield_from():
        yield from 'AB'
        yield from range(5)

`yield from x` calls `iter(x)` to obtain an iterator from it.

However, `yield from` can do more than this.
`yield from` is a syntax for delegating to a subgenerator.
It can open a bidirectional channel from the outermost caller to the innermost subgenerator so values can be yielded back and forth between them, and exceptions thrown without too much boilercode for handling.
This enables coroutine delegation.

First, we need to get some concept down:

- `delegating generator` - the generator that contains the `yield from <iterable>` expression
- `subgenerator` - the generator obtained from the `<iterable>` part of the yield expression
- `caller` or `client` - the caller is the client code that calls the delegating generator

.. code-block:: python

    from collections import namedtuple


    Result = namedtuple('Result', 'count average')


    # the subgenerator
    def averager():
        total = 0.0
        count = 0
        average = None
        while True:
            term = yield
            if term is None:
                break
            total += term
            count += 1
            average = total/count
        return Result(count, average)


    # the delegating generator
    def grouper(results, key):
        while True:
            results[key] = yield from averager()


    # the caller, aka. client
    def main(data):
        results = {}
        for key, values in data.items():
            group = grouper(results, key)
            next(group)
            for value in values:
                group.send(value)
            group.send(None) # important!
        # print(results)
        report(results)


    # output results
    def report(results):
        for key, result in sorted(results.items()):
            group, unit = key.split(';')
            print(f'{result.count:2} {group:5} averaging {result.average:.2f}')


    data = {
        'girls;kg':
            [40.9, 38.5, 44.3, 42.2, 45.2, 41.7, 44.5, 38.8, 40.6, 44.5],
        'girls;m':
            [1.6, 1.51, 1.4, 1.3, 1.41, 1.39, 1.33, 1.46, 1.45, 1.43],
        'boys;kg':
            [39.0, 40.8, 43.2, 40.8, 43.1, 38.6, 41.4, 40.6, 36.3],
        'boys;m':
            [1.38, 1.5, 1.32, 1.25, 1.37, 1.48, 1.25, 1.49, 1.46]
    }

    if __name__ == '__main__':
        main(data)

The subgenerator is the same `averager` function we have already seen.
We use the one with the `return` statement to demonstrate how the `StopIteration` is handled by the delegating generator.

There are definitely easier ways of doing this computation, but that's not the point.

The point of this setup is to show how `send` is tunneled through the delegating generator from `main` to `averager`. 

Running the program above gives:

.. code-block:: text

    9 boys  averaging 40.42
    9 boys  averaging 1.39
    10 girls averaging 42.12
    10 girls averaging 1.43

If this all seems a bit mysterious, we can see what's going on if we uncomment the print statement in `main`:

.. code-block:: text

    {
        'girls;kg': Result(count=10, average=42.120000000000005), 
        'girls;m': Result(count=10, average=1.4279999999999997), 
        'boys;kg': Result(count=9, average=40.422222222222224), 
        'boys;m': Result(count=9, average=1.3888888888888888)
    }
     9 boys  averaging 40.42
     9 boys  averaging 1.39
    10 girls averaging 42.12
    10 girls averaging 1.43

.. note:: Try to strategically put some print statements throughout the code to better your understanding. Also, what happens if you comment out the line marked *important!*?

The construct you have just seen is much more powerful than it might seem on the surface.
You can have any number of delegating generators tunnelling data.
Don't despair if all this seems abstract - it's a bit like quantun mechanics - if you think you understand everything, you probably don't understand anything.

Coroutines are a natural and powerful way of expressing many algorithms, such as in simulation, games, asynchronous I/O and anything that is event-driven by nature.

Use Case: Coroutines for Discrete Event Simulation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Now let's try something fun.
We will build a taxi simulator without the use of threads - instead, we will use a coroutine.

The taxi simulator is an example of a *Discrete Event Simulator* (DES).
A DES can be used to model a sequence of events.
It is purely event-based, so there is no notion of a clock that advances in fixed increments.

A DES can be used for turn-based-games: the state of the game advances as a player makes a move (an event), rather than by time.
Many problems can be simulated this way, and if you are interested in that, you can take a look at `Simpy` which is also implemented using coroutines.

You might wonder why a coroutine would be needed for this - you could just have a lot of if-statements, right?
Coroutines allow for an arbitrary number of events to be able to take control.
It allows for slow processes outside the system to activate a coroutine when ready.
Also, it avoids threads and many of the problems (such as deadlocks and race conditions) that you need to take care of with threads.

In a DES, a *process* refers to the activities of an entity (e.g. a player or non-player-character) in the model, not to an OS process.
The term is used in the program listing below as the activities of the taxies (the entities).

The listing below is quite long, but most of it is pretty trivial.
The important parts to take notice of are `taxi_process` (the coroutine) and `Simulator.run` that holds the main event loop.

Enough with the explanations, let's see some code:

.. code-block:: python

    import random
    import collections
    import queue
    import argparse
    import time


    DEFAULT_NUMBER_OF_TAXIS = 3
    DEFAULT_END_TIME = 180
    SEARCH_DURATION = 5
    TRIP_DURATION = 20
    DEPARTURE_INTERVAL = 5

    Event = collections.namedtuple('Event', 'time proc action')


    def taxi_process(ident, trips, start_time=0):
        """Yield to simulator issuing event at each state change"""
        time = yield Event(start_time, ident, 'leave garage')
        for i in range(trips):
            time = yield Event(time, ident, 'pick up passenger')
            time = yield Event(time, ident, 'drop off passenger')
        yield Event(time, ident, 'going home')


    class Simulator:

        def __init__(self, procs_map):
            self.events = queue.PriorityQueue()
            self.procs = dict(procs_map)

        def run(self, end_time):
            """Schedule and display events until time is up"""

            # prime and first event
            for _, proc in sorted(self.procs.items()):
                first_event = next(proc)
                self.events.put(first_event)

            # main loop for sim
            sim_time = 0
            while sim_time < end_time:
                if self.events.empty():
                    print('### end of events ###')
                    break

                current_event = self.events.get()
                sim_time, proc_id, previous_action = current_event
                print(f'# taxi {proc_id} {proc_id * "   "} {current_event}')
                active_proc = self.procs[proc_id]
                next_time = sim_time + compute_duration(previous_action)
                try:
                    next_event = active_proc.send(next_time)
                except StopIteration:
                    del self.procs[proc_id]
                else:
                    self.events.put(next_event)
            else:
                print(f'### end of simulation time: {self.events.qsize()} events pending ###')


    def compute_duration(previous_action):
        """Compute action duration using exponential distribution"""
        if previous_action in ['leave garage', 'drop off passenger']:
            # new state is prowling
            interval = SEARCH_DURATION
        elif previous_action == 'pick up passenger':
            # new state is trip
            interval = TRIP_DURATION
        elif previous_action == 'going home':
            interval = 1
        else:
            raise ValueError(f'# Unknown previous action {previous_action}')
        return int(random.expovariate(1/interval)) + 1


    def main(end_time=DEFAULT_END_TIME, num_taxis=DEFAULT_NUMBER_OF_TAXIS, seed=None):
        """Initialize random generator, build procs and build simulation"""
        if seed is not None:
            random.seed(seed)

        taxis = {i: taxi_process(i, (i+1)*2, i * DEPARTURE_INTERVAL)
                for i in range(num_taxis)}
        sim = Simulator(taxis)
        sim.run(end_time)


    if __name__ == '__main__':
        parser = argparse.ArgumentParser(description='Taxi fleet simulator')
        parser.add_argument('-e', '--end-time', type=int, default=DEFAULT_END_TIME, help=f'simulation end time; default={DEFAULT_END_TIME}')
        parser.add_argument('-t', '--taxis', type=int, default=DEFAULT_NUMBER_OF_TAXIS, help=f'number of taxis running; default={DEFAULT_NUMBER_OF_TAXIS}')
        parser.add_argument('-s', '--seed', type=int, default=None, help=f'random generator seed (for testing)')

        args = parser.parse_args()
        main(args.end_time, args.taxis, args.seed)

Running the program gives an output like this (remember, it's random based, your output is likely different):

.. code-block:: text

    # taxi 0  Event(time=0, proc=0, action='leave garage')
    # taxi 0  Event(time=1, proc=0, action='pick up passenger')
    # taxi 1     Event(time=5, proc=1, action='leave garage')
    # taxi 1     Event(time=6, proc=1, action='pick up passenger')
    # taxi 1     Event(time=8, proc=1, action='drop off passenger')
    # taxi 1     Event(time=9, proc=1, action='pick up passenger')
    # taxi 2        Event(time=10, proc=2, action='leave garage')
    # taxi 2        Event(time=13, proc=2, action='pick up passenger')
    # taxi 1     Event(time=14, proc=1, action='drop off passenger')
    # taxi 0  Event(time=15, proc=0, action='drop off passenger')
    # taxi 0  Event(time=17, proc=0, action='pick up passenger')
    # taxi 1     Event(time=20, proc=1, action='pick up passenger')
    # taxi 2        Event(time=20, proc=2, action='drop off passenger')
    # taxi 1     Event(time=22, proc=1, action='drop off passenger')
    # taxi 1     Event(time=24, proc=1, action='pick up passenger')
    # taxi 2        Event(time=27, proc=2, action='pick up passenger')
    # taxi 1     Event(time=29, proc=1, action='drop off passenger')
    # taxi 0  Event(time=31, proc=0, action='drop off passenger')
    # taxi 1     Event(time=33, proc=1, action='going home')
    # taxi 2        Event(time=35, proc=2, action='drop off passenger')
    # taxi 0  Event(time=44, proc=0, action='going home')
    # taxi 2        Event(time=44, proc=2, action='pick up passenger')
    # taxi 2        Event(time=51, proc=2, action='drop off passenger')
    # taxi 2        Event(time=52, proc=2, action='pick up passenger')
    # taxi 2        Event(time=60, proc=2, action='drop off passenger')
    # taxi 2        Event(time=61, proc=2, action='pick up passenger')
    # taxi 2        Event(time=74, proc=2, action='drop off passenger')
    # taxi 2        Event(time=75, proc=2, action='pick up passenger')
    # taxi 2        Event(time=79, proc=2, action='drop off passenger')
    # taxi 2        Event(time=81, proc=2, action='going home')
    ### end of events ###

The level of indention is based on the taxi (proc).

Now you should have a good idea about what coroutines are.
Coroutines are build on top of generators - they allow for event-driven programming.

Actually, coroutines are an example of *cooperative multitasking* (as opposed to *preempetive multitasking*).
It is cooperative because it up to the coroutine to give up control (on yield) to the caller.

Next week we will dive more into the application of coroutines with asynchronous programming.

.. note:: Based on the taxi simulator example, can you build a simple simulator for some other problem?

.. note:: There are a few issues with the program above, amongst them dependence on string labels. Can you make a better implementation?
